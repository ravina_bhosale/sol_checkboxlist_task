﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_CheckBoxList.Models
{
    public class HobbyEntity
    {
        public decimal? HobbyId { get; set; }

        public String HobbyName { get; set; }

        public Boolean? Interested { get; set; }

        public decimal? UserId { get; set; }
    }
}