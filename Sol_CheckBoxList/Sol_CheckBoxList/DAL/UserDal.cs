﻿using Sol_CheckBoxList.DAL.ORD;
using Sol_CheckBoxList.Models;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_CheckBoxList.DAL
{
    public class UserDal
    {
        #region Declaration
        private UserDcDataContext dc = null;
        #endregion

        #region constructor
        public UserDal()
        {
            dc = new UserDcDataContext();
        }
        #endregion

        #region public Method
        public async Task<dynamic> AddUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;
            decimal? id = null;

            try
            {
                return await Task.Run(async () =>
                {
                    var setQuery =
                    dc
                    ?.uspSetUser
                    (
                        "Add",
                        userEntityObj?.UserId,
                        userEntityObj?.FirstName,
                        userEntityObj?.LastName,
                        ref status,
                        ref message,
                        ref id
                     );

                    // Bind Id to Existing user Entity Data
                    userEntityObj.UserId = id;

                    // add Hobby Data 
                    await this.AddUserHobbyData(userEntityObj);

                    return setQuery;
                });
            }

            catch (Exception)
            {
                throw;
            }
        }

        public async Task<UserEntity> GetUserData(UserEntity userEntityObj)
        {
            return await Task.Run(() => {

                return

                  dc
                  ?.uspGetUsers(
                      "UserData",
                      userEntityObj.UserId
                      )
                  ?.AsEnumerable()
                  ?.Select(
                        (leUspGetUsersResultSetObj) => new UserEntity()
                        {
                            UserId = leUspGetUsersResultSetObj.UserId,
                            FirstName = leUspGetUsersResultSetObj.FirstName,
                            LastName = leUspGetUsersResultSetObj.LastName,

                            hobby = (this.GetUserHobbyData(new HobbyEntity()
                            {
                                UserId = leUspGetUsersResultSetObj.UserId
                            })).Result as List<HobbyEntity>
                        })

                    ?.ToList()
                    ?.FirstOrDefault();



            });
        }


        public async Task<dynamic> UpdateUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;
            decimal? id = null;

            try
            {
                return await Task.Run(async () =>
                {
                    var setQuery =
                    dc
                    ?.uspSetUser
                    (
                        "Update",
                        userEntityObj?.UserId,
                        userEntityObj?.FirstName,
                        userEntityObj?.LastName,
                        ref status,
                        ref message,
                        ref id
                     );

                    // add Hobby Data 
                    await this.UpdateUserHobbyData(userEntityObj);

                    return setQuery;
                });
            }

            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Property
        private Func<uspGetUsersResultSet, HobbyEntity> SelectUserHobbyData
        {
            get
            {
                return
                       (leUspGetUsersResultSetObj) => new HobbyEntity()
                       {
                           HobbyId = leUspGetUsersResultSetObj.HobbyId,
                           HobbyName = leUspGetUsersResultSetObj.HobbyName,
                           Interested= leUspGetUsersResultSetObj.Interested
                       };
            }
        }



        #endregion

        #region Private Method
        private async Task AddUserHobbyData(UserEntity userentityObj)
        {
            int? status = null;
            string message = null;

            await Task.Run(() =>
            {

                foreach (HobbyEntity hobbyObj in userentityObj.hobby)
                {
                    dc
                    ?.uspSetHobby
                    (
                        "Insert",
                        hobbyObj?.HobbyId,
                        hobbyObj?.HobbyName,
                        hobbyObj?.Interested,
                        userentityObj?.UserId,
                        ref status,
                        ref message
                     );
                }
            });
        }


        private async Task UpdateUserHobbyData(UserEntity userentityObj)
        {
            int? status = null;
            string message = null;

            await Task.Run(() =>
            {

                foreach (HobbyEntity hobbyObj in userentityObj.hobby)
                {
                    dc
                    ?.uspSetHobby
                    (
                        "Update",
                        hobbyObj?.HobbyId,
                        hobbyObj?.HobbyName,
                        hobbyObj?.Interested,
                        userentityObj?.UserId,
                        ref status,
                        ref message
                     );
                }
            });
        }

        private async Task<IEnumerable<HobbyEntity>> GetUserHobbyData(HobbyEntity userHobbyEntityObj)
        {
            return await Task.Run(() => {

                return
                    dc
                    ?.uspGetUsers(
                        "UserHobbyData",
                        userHobbyEntityObj.UserId
                        )
                    ?.AsEnumerable()
                    ?.Select(this.SelectUserHobbyData)
                    ?.ToList();

            });
        }

        #endregion
    }
    }