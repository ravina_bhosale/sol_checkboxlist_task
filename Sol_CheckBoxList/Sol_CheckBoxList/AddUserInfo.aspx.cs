﻿using Sol_CheckBoxList.DAL;
using Sol_CheckBoxList.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_CheckBoxList
{
    public partial class AddUserInfo : System.Web.UI.Page
    {

        #region Declaration
        private UserDal userDalObj = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        #region Private Property
        private String BindLabelMessage
        {
            get
            {
                return lblMessage.Text;
            }
            set
            {
                lblMessage.Text = value;
            }
        }

        //First create property for checkboxlist
        private List<HobbyEntity> GetHobbyData
        {
            get
            {
                return chkHobby
                     .Items
                     .Cast<ListItem>()
                     .AsEnumerable()
                     .Select((leCheckBoxListObj) => new HobbyEntity()
                     {
                         HobbyName = leCheckBoxListObj.Text,
                         Interested = leCheckBoxListObj.Selected
                     })
                    .ToList();
            }
        }

        #endregion

        #region Private Method

        //Bind data UI to Entity 
        private async Task<UserEntity> UserMappingData()
        {
            return await Task.Run(() =>
            {
                var userEntityObj = new UserEntity()
                {
                    FirstName = txtFirstName.Text,
                    LastName = txtLastName.Text,
                    hobby = this.GetHobbyData//bind checkboxlist property
                };
                return userEntityObj;
            });
        }

        private async Task AddUserData(UserEntity userEntityObj)
        {
            // Create an instance of User Dal.
            userDalObj = new UserDal();

            // add Data to Database and Bind to the lable
            this.BindLabelMessage = await userDalObj.AddUserData(userEntityObj);
        }

#endregion

        protected async void btnSubmit_Click(object sender, EventArgs e)
        {
            // Mapping User Entity Obj.
            var userEntityObj = await this.UserMappingData();

            // Add data
            await AddUserData(userEntityObj);
        }
    }
}