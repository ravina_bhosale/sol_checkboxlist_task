﻿CREATE PROCEDURE [dbo].[uspSetUser]
(	@Command Varchar(MAX)=NULL,
	
	@UserId numeric(18,0)=NULL,
	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)= NULL OUT,
	@Id Numeric(18,0)=NULL OUT
)
AS
	
	BEGIN
		
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='Add'
			BEGIN
				
				BEGIN TRANSACTION

				BEGIN TRY
					
					INSERT INTO tblUser
					(
						
						FirstName,
						LastName
					)
					VALUES
					(
						@FirstName,
						@LastName
					)

					SELECT @Id =@@IDENTITY

					SET @Status=1
					SET @Message='Insert Success'

					 

					COMMIT TRANSACTION

				END TRY

				BEGIN CATCH
					
					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='Stored Proc execution Fail'

					RAISERROR(@ErrorMessage,16,1)
					
					ROLLBACK TRANSACTION

				END CATCH 

			END 

			ELSE IF @Command='Update'
			BEGIN 
						
					BEGIN TRANSACTION

					BEGIN TRY 

						SELECT 
						@FirstName=CASE WHEN @FirstName IS NULL THEN U.FirstName ELSE @FirstName END,
						@LastName=CASE WHEN @LastName IS NULL THEN U.LastName ELSE @LastName END
						FROM tblUser AS U
							WHERE U.UserId=@UserId

							UPDATE tblUser
								SET 
									FirstName=@FirstName,
									LastName=@LastName
										WHERE UserId=@UserId

									SELECT @Id =@@IDENTITY
										
								SET @Status=1
								SET @Message='Update Succesfully'

								COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Update Exception'

						RAISERROR(@ErrorMessage,16,1)
					END CATCH


			END

	END